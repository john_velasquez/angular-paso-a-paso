import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges, OnDestroy } from '@angular/core';
import { IntervalService } from '../../services/interval.service';
import { Subscription } from 'rxjs';
@Component({
  selector: 'ap-temporizador',
  templateUrl: './temporizador.component.html',
  styleUrls: ['./temporizador.component.css']
})
export class TemporizadorComponent implements OnInit, OnChanges, OnDestroy {

  @Input()
  value: number;

  @Output()
  complete = new EventEmitter<any>();

  interval: Subscription;

  constructor( private intervalService: IntervalService) {
    /*this.value = 10;*/
  }

  ngOnInit() {
    alert('hola');
    this.iniciar();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.value) {
      this.reiniciar();
    }
  }

  ngOnDestroy() {
    alert('adios');
    this.terminar();
  }

  reiniciar() {
    this.terminar();
    this.iniciar();
  }

  iniciar() {
    if (this.value) {
      this.interval = this.intervalService.createInterval(1000).subscribe((d) => {
        console.log(d);
        this.value = this.value <= 0 ? 0 : this.value - 1;
        if (this.value <= 0) {
          this.terminar();
        }
      });
    }
  }

  terminar() {
    if (this.interval) {
      this.interval.unsubscribe();
      this.interval = undefined;
      if (this.value <= 0) {
        this.complete.emit();
      }
    }
  }
}

import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ut-led',
  templateUrl: './led.component.html',
  styleUrls: ['./led.component.css'],
  host: {'[style.background]': 'color'}
})
export class LedComponent implements OnInit {

  @Input()
  frecuencia: number;

  @Input()
  color: string;

  colorInactivo: '#f9f9f9';

  constructor() { }

  ngOnInit() {
    this.color = this.color || 'red';
  }

}

import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'ut-leds',
  templateUrl: './leds.component.html',
  styleUrls: ['./leds.component.css']
})
export class LedsComponent implements OnInit {

  constructor( private location: Location) { }

  ngOnInit() {
  }

  atras() {
    this.location.back();
  }

}

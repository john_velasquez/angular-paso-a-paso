import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LedsComponent } from './components/leds/leds.component';

const routes: Routes = [
  { path: 'utils/led', component: LedsComponent  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UtilsRoutingModule { }

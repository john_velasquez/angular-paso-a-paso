import { Directive, Input, HostBinding, OnInit, OnDestroy } from '@angular/core';
import { IntervalService } from '../../services/interval.service';
import { Subscription } from 'rxjs';

@Directive({
  selector: '[utParpadeante]'
})
export class ParpadeanteDirective implements OnInit, OnDestroy {

  @Input()
  colorActivo: string;

  @Input()
  colorInactivo: string;

  @HostBinding('style.backgroundColor')
  color: string;

  @Input()
  frecuencia: number;

  private subscripcion: Subscription;
  constructor(private intevalService: IntervalService) { }

  ngOnInit() {
    if (this.frecuencia > 0) {
      this.subscripcion = this.intevalService.createInterval(this.frecuencia).subscribe(() => this.toggleColor());
    }
  }

  ngOnDestroy() {
    if (this.subscripcion){
      this.subscripcion.unsubscribe();
    }
  }

  toggleColor() {
    this.color = this.color === this.colorActivo ? this.colorInactivo : this.colorActivo;
  }

}

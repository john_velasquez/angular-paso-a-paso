import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UtilsRoutingModule } from './utils-routing.module';
import { LedComponent } from './components/led/led.component';
import { ParpadeanteDirective } from './directives/parpadeante.directive';
import { LedsComponent } from './components/leds/leds.component';

@NgModule({
  imports: [
    CommonModule,
    UtilsRoutingModule
  ],
  declarations: [LedComponent, ParpadeanteDirective, LedsComponent],
  exports: [LedComponent]
})
export class UtilsModule { }

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { TemporizadorComponent } from './componentes/temporizador/temporizador.component';
import { IntervalService } from './services/interval.service';
import { AlertOnCompleteDirective } from './directives/alert-on-complete.directive';
import { UtilsModule } from './utils/utils.module';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './componentes/home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    TemporizadorComponent,
    AlertOnCompleteDirective,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    UtilsModule,
    AppRoutingModule
  ],
  providers: [IntervalService],
  bootstrap: [AppComponent]
})
export class AppModule { }

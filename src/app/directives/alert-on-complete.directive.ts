import { Directive } from '@angular/core';

@Directive({
  selector: '[apAlertOnComplete]',
  host:{
    '(complete)': 'alerComplete()'
  }
})
export class AlertOnCompleteDirective {

  constructor() { }

  alerComplete() {
    alert('Listo');
  }
}

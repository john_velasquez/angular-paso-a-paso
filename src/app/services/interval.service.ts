import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class IntervalService {

  constructor() { }

  createInterval(interval): Observable<number> {
    return Observable.interval(interval);
  }

}
